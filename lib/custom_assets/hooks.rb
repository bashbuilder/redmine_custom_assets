class CustomAssets::Hooks < Redmine::Hook::ViewListener
  ASSETS = File.join File.dirname(__FILE__), '..', '..', 'assets'
  PLUGIN = 'custom_assets'

  # Custom styles
  def view_layouts_base_html_head(context={})
    res = []
    Dir.chdir(File.join ASSETS, 'stylesheets') do
      Dir.glob '*.css' do |style|
        res << stylesheet_link_tag(style.gsub(/\.css$/, ''), :plugin => PLUGIN)
      end
    end
    res.join
  end

  # Custom JS at body bottom
  def view_layouts_base_body_bottom(context={})
    res = []
    Dir.chdir(File.join ASSETS, 'javascripts') do
      Dir.glob '*.js' do |js|
        res << javascript_include_tag(js.gsub(/\.js$/, ''), :plugin => PLUGIN)
      end
    end
    res.join
  end

  def view_issues_form_details_bottom(context={})
    res = []
    Dir.chdir(File.join ASSETS, 'javascripts', 'issue_form') do
      Dir.glob '*.js' do |js|
        res << javascript_include_tag("issue_form/#{js.gsub(/\.js$/, '')}", :plugin => PLUGIN)
      end
    end
    res.join
  end
end

